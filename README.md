# Datastore

## Functionality
The datastore can process real-time streaming data as well as static datasets. 
It uses a Vertx Cluster to run Connector Verticles to fetch real-time data sources, and a Webserver Verticle for users to upload datasets.
The incoming real-time data gets forwarded through Apache Kafka, processed with Spark Streaming and saved to an HDFS Cluster. 
The static datasets can be uploaded to HDFS directly. An HTTP and Websocket Server is running also in Vertx to provide the processed data.


## Prerequisites
* Docker (Production & Development)
* Docker-compose (Production & Development)
* Maven (Development)
* Java (Development)y

## Build Instructions
To build and start the project execute the following command in a shell from the project folder.

1. Start Proprietary Projects by executing: `docker-compose up`
2. Build the project by executing: `mvn clean package`
3. Start the Harvester by executing: `java -jar targest/Harvester.jar`

Afterwards the Harvester is starting to collect data. To debug if data is actually written into Kafka. 
You can attach to the Kafka container by executing the following command in a shell:

```docker exec -it kafka /bin/bash```

You can then use the commands explained in [this tutorial](http://cloudurable.com/blog/kafka-tutorial-kafka-from-command-line/index.html) to look for Topics or Messages on kafka:
For example, to view all topics:

```bash
cd /opt/bitami/kafka/bin
./kafka-topics.sh --list --zookeeper zookeeper:2181
```

Likewise, to view all messages for the Discovergy harvester:

```bash
cd /opt/bitami/kafka/bin
./kafka-console-consumer.sh --bootstrap-server localhost:9093 --topic Inbound.Discovergy --from-beginning
```

## Configuration

The harvesters can be configured in two ways.
A default configuration is provided in the `conf/default_config.json` file, which must be renamed to `config.json` if this type of configuration is desired.
Alternatively, the application may configured using environment variables. These will overwrite the file-based configuration if both are present.
Only those harvesters for which the corresponding configuration is present will be deployed, whereas the Kafka config is always required. 
The table below shows the available environment variables and examples of their corresponding JSON configuration. 

| Key | Example |
| :-- | :--     |
| PIVEAU_DATASTORE_HARVESTER_KAFKA_CONFIG | `{ "bootstrap.servers":"localhost:9092", "key.serializer":"org.apache.kafka.common.serialization.StringSerializer", "value.serializer":"org.apache.kafka.common.serialization.StringSerializer", "value.deserializer":"org.apache.kafka.common.serialization.StringDeserializer", "key.deserializer":"org.apache.kafka.common.serialization.StringDeserializer", "partitioner.class":"io.piveau.DataStore.Commons.schema.DataStorePartitioner", "auto.offset.reset": "latest", "enable.auto.commit": "false","metadata.max.age.ms": "2000" }`|
| DISCOVERGY_CONFIG | `{ "host": "api.discovergy.com", "user": "myUser@example.com", "password": "changeMe123", "fetchIntervalSeconds": 5, "earliestLookupTimestamp": 1604185200000 }` |
| DWD_CONFIG | `{ "host": "opendata.dwd.de","path": "/weather/text_forecasts/tables/","cityIds": ["FDBJ06_FXXX"],"downloadWindowHours" : 6,"checkIntervalSeconds": 600,"hours": [0, 2, 5, 8, 11, 14, 17, 20, 23] }` || OPEN_WEATHER_MAP_CONFIG | `{ "topic": "inbound.OpenWeatherMap","keyPrefix": "OpenWeatherMap:","fetchIntervalSeconds": 50,"url": "http://api.openweathermap.org/data/2.5/weather","cityId": "2842884","appId": "myAppId" }` |
| OPEN_WEATHER_MAP_CONFIG | `{ "fetchIntervalInSeconds": 60, "url": "http://api.openweathermap.org/data/2.5/weather", "cityId": "2842884", "appId": "myAppId" }` |
| TEST_CONFIG | `{ "fetchIntervalInSeconds": 60 }` |
| URBAN_INSTITUTE_CONFIG | `{ "reconnectMS": 60000,"host": "portal.quartier-der-zukunft.de","port": 443,"user": "myUser","password": "changeMe123","statementIds": ["3473f6e1-e86f-4a57-95eb-3727fb25b70b"] }` |
| RMV_CONFIG | `{ "accessId": "myAccessId", "region": "Rüsselsheim", "maxNumberOfStations": 1000, "fetchIntervalInSeconds": 60 }` |


## Usage
The harvester collects data from external data sources and forwards it to Kafka.
Each external source has its own verticle in the Harvester application. A verticle is just a class which extends AbstractVerticle.

```java
public class TestHarvester extends AbstractVerticle {

    @Override
    public void start() {
    }

    public void startHarvesting(Map<String, String> kafkaConfig) {
        producer = KafkaProducer.create(vertx, kafkaProducerConfig);
        faker = new Faker();

        fetchNewDataId = vertx.setPeriodic(fetchIntervalInSeconds * 1000, id -> {
            // Create Request
            JsonObject message = new JsonObject()
                    .put("id", UUID.randomUUID().hashCode())
                    .put("userId", UUID.randomUUID().hashCode())
                    .put("title", faker.lorem().word())
                    .put("firstname", faker.name().firstName())
                    .put("lastname", faker.name().lastName())
                    .put("body", faker.lorem().paragraph())
                    .put("timestamp", LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));

            // Write to Kafka
            KafkaProducerRecord<String, String> record =
                    KafkaProducerRecord.create(KafkaTopics.INBOUND_TESTAPI.topicName, KafkaKeys.INBOUND_TESTAPI_KEY.keyName, message.encode());

            producer.write(record, write -> {
                if (write.succeeded()) {
                    log.debug("Wrote message to Kafka");
                } else {
                    log.error("Could not write message to Kafka", write.cause());
                }
            });
        });
    }
}
```

The *start* methods retrieves the KafkaConfig in form of a Map. 
It then creates the KafkaProducer and repeatedly harvests data. 
The harvesting process itself as the TestHarvester is a mocked harvester skips the actual data retrieval process and instead creates one test message with an external library called Faker in json format.
The lower part of the functions writes the mocked data to kafka by producing a *KafkaProducerRecord*. Topic and Key comes from the KafkaTopic and KafkaKey implementations of the **Commons** module.