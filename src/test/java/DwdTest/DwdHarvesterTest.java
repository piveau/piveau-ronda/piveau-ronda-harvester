package DwdTest;


import io.piveau.DataStore.Harvester.util.DwdParser;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.junit5.Checkpoint;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;


@ExtendWith(VertxExtension.class)
public class DwdHarvesterTest {
    private static final Logger log = LoggerFactory.getLogger(DwdHarvesterTest.class);

    private Map schemaList = new HashMap<String, JsonObject>();

    @Test
    @DisplayName("Test DWD Schema")
    public void testDwdSchema(Vertx vertx, VertxTestContext testContext) {
        Checkpoint passTest = testContext.checkpoint(3);

        CompositeFuture.all(List.of(
                getSChemaByName("lorem.txt", vertx),
                getSChemaByName("schema1.txt", vertx),
                getSChemaByName("schema2.txt", vertx)))
                .onFailure(fail -> testContext.failNow(fail.getCause()))
                .onSuccess(success -> {

                    // Test lorem ipsum
                    DwdParser lorem = new DwdParser(this.schemaList.get("lorem").toString());
                    JsonObject loremObject = lorem.extractTable();
                    assertEquals(0, loremObject.size());
                    passTest.flag();

                    // Test schema1
                    DwdParser schema1 = new DwdParser(this.schemaList.get("schema1").toString());
                    JsonObject schema1Object = schema1.extractTable();
                    assertEquals(true, schema1Object .size() > 0);
                    passTest.flag();

                    // Test schema1
                    DwdParser schema2 = new DwdParser(this.schemaList.get("schema2").toString());
                    JsonObject schema2Object = schema2.extractTable();
                    assertEquals(true, schema2Object.size() > 0);
                    passTest.flag();
                });

    }

    private Future<Void> getSChemaByName(String filename, Vertx vertx) {
        return Future.future(readFileFuture -> {
            vertx.fileSystem().readFile("dwdSchema/" + filename, readResult -> {
                if (readResult.succeeded()) {
                    this.schemaList.put(filename.replace(".txt", ""), readResult.result().toString());
                    readFileFuture.complete();
                } else {
                    log.info("Failed to get file with reason: {}", readResult.cause().getMessage());
                    readFileFuture.fail(readResult.cause().getMessage());
                }
            });
        });
    }

}
