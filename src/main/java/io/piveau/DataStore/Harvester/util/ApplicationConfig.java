package io.piveau.DataStore.Harvester.util;

public final class ApplicationConfig {
    public static final String ENV_KAFKA_CONFIG = "PIVEAU_DATASTORE_HARVESTER_KAFKA_CONFIG";

    public static final String ENV_DISCOVERGY_CONFIG = "DISCOVERGY_CONFIG";
    public static final String ENV_DWD_CONFIG = "DWD_CONFIG";
    public static final String ENV_OPEN_WEATHER_MAP_CONFIG = "OPEN_WEATHER_MAP_CONFIG";
    public static final String ENV_RMV_CONFIG = "RMV_CONFIG";
    public static final String ENV_TEST_CONFIG = "TEST_CONFIG";
    public static final String ENV_URBAN_INSTITUTE_CONFIG = "URBAN_INSTITUTE_CONFIG";

    public static final String PATH_JSON_CONFIG = "conf/config.json";

    public static long DEFAULT_FETCH_INTERVAL_IN_SECONDS = 1000;
}
