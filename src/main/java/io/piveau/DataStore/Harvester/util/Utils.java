package io.piveau.DataStore.Harvester.util;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.piveau.DataStore.Commons.schema.base.Key;
import io.vertx.core.json.JsonObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Base64;
import java.util.Map;
import java.util.stream.Collectors;

public class Utils {
    private static final ObjectMapper mapper = new ObjectMapper()
            .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
            .setSerializationInclusion(JsonInclude.Include.NON_NULL)
            .setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZZZZ"));


    public static String BasicAuthEncode(String user, String password) {
        return "Basic " + Base64.getEncoder().encodeToString((user + ":" + password).getBytes());
    }

    public static String mapToKeyStructure(String jsonString, Key<?> key) throws IOException {
        return mapper.writeValueAsString(mapper.readValue(jsonString, key.schema));
    }

    public static <T> JsonObject mapFromKeyStructure(T t) throws IOException {
        return new JsonObject(mapper.writeValueAsString(t));
    }

    public static Map<String, String> getKafkaConsumerConfig(JsonObject config) {
        Map<String, String> kafkaConfig = getKafkaConfig(config);

        Arrays.asList("value.serializer", "partitioner.class", "key.serializer")
                .forEach(kafkaConfig::remove);

        return kafkaConfig;
    }

    public static Map<String, String> getKafkaProducerConfig(JsonObject config) {
        Map<String, String> kafkaConfig = getKafkaConfig(config);

        Arrays.asList("group.id", "value.deserializer", "key.deserializer", "auto.offset.reset", "enable.auto.commit")
                .forEach(kafkaConfig::remove);

        return kafkaConfig;
    }

    private static Map<String, String> getKafkaConfig(JsonObject config) {
        return config.getJsonObject(ApplicationConfig.ENV_KAFKA_CONFIG).stream()
                .collect(Collectors.toMap(Map.Entry::getKey, e -> e.getValue().toString()));
    }
}
