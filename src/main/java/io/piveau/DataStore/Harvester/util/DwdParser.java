package io.piveau.DataStore.Harvester.util;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Info: Some lines are put in comment as their are not in the intersection of both schemas
 * But kept in code if the use is intended
 */

public class DwdParser {
    private static final Logger log = LoggerFactory.getLogger(DwdParser.class);

    private String inputString;
    private List<String> tableString;

    public DwdParser(String inputString) {
        this.inputString = inputString;
    }

    public JsonObject extractTable() {
        tableString = new ArrayList<>();
        JsonObject content = new JsonObject();
        // Created at
        Pattern patternCreatedAt = Pattern.compile("(\\W*(zum)\\W*(\\d{2}\\.\\d{2}\\.\\d{4})\\,\\s{1}\\d{2})|(\\W*(am)\\W*(\\d{2}\\.\\d{2}\\.\\d{4}\\s\\d{2}\\:\\d{2}))|((\\W*(am)\\W*(\\d{2}\\.\\d{2}\\.\\d{2},\\s\\d{2}\\:\\d{2})))|(\\W*(am)\\W*(\\d{2}\\.\\d{2}\\.\\d{4},\\s\\d{2}\\:\\d{2}))|(\\W*den\\W*\\s{1}\\d{2}\\.\\d{2}\\.\\d{4}\\s\\W*um\\W*\\s{1}\\d{2}\\:\\d{2})");
        for (String line : this.inputString.split("\n")) {
            if (patternCreatedAt.matcher(line).find()) {
                String s_date = extractPattern(Pattern.compile("(\\d{2}\\.\\d{2}\\.\\d{4})|(\\d{2}\\.\\d{2}\\.\\d{4})|(\\d{2}\\.\\d{2}\\.\\d{2})"), line);
                String s_time = extractPattern(Pattern.compile("(\\d{2}\\:\\d{2})|(\\d{2}\\s{1}\\W*(Uhr))"), line);
                if (s_date.isEmpty() || s_time.isEmpty()) {
                    log.info("Could not find date in line {}", line);
                    return new JsonObject();
                }
                String[] date = s_date.split("\\.");
                String createdAt = date[2] + "-" + date[1] + "-" + date[0] + "T" + s_time + ":00.000+0100";
                String Timestamp = date[2] + "-" + date[1] + "-" + date[0] + "T01:00:00.000+0100";
                content
                        .put("timestamp", Timestamp)
                        .put("createdAt", createdAt)
                        .put("forecast", new JsonArray());

            }
        }

        if (!content.containsKey("timestamp") || !content.containsKey("createdAt")) {
            log.error("Could not extract timestamp for table {}", inputString);
            //return new JsonArray();
        }


        // Schema1
        // Following command extracts Maximale, Minimale Forecasts := Pattern pattern = Pattern.compile("((\\s{3,}\\d{1,2})|(\\(\\D{3,5}\\))|(\\s{3,}\\D\\s{3,})|(\\bZustand\\s+)|(\\bStreckentyp)|(\\bmaximale\\s+)|(\\bmittlere\\s+)|(\\bminimale\\s+))");
        Pattern pattern = Pattern.compile("((\\s{3,}\\d{1,2})|(\\(\\D{3,5}\\))|(\\s{3,}\\D\\s{3,})|(\\bZustand\\s+)|(\\bStreckentyp)|(\\bmittlere\\s+))");
        for (String line : this.inputString.split("\n")) {
            Matcher match = pattern.matcher(line);
            if (match.find() && !line.contains("von") && !line.contains("z.B.") && !line.contains("|") && !line.contains("Zur Darstellung") && !tableString.contains(line)) {
                tableString.add(line);
            }
        }

        // Schema 2
        if (tableString.size() == 0) {
            pattern = Pattern.compile("\\d{1,3}\\s\\|\\s*.*");
            for (String line : this.inputString.split("\n")) {
                Matcher match = pattern.matcher(line);
                if (match.find() && !tableString.contains(line)) {
                    tableString.add(line);
                }
            }
            if (tableString.size() == 0) {
                log.warn("Could not extract table from Forecast");
                return new JsonObject();
            } else return extractSchema1(tableString, content);
        } else return extractSchema2(tableString, content);
    }

    private JsonObject extractSchema1(List<String> tableString, JsonObject content) {
        JsonObject result = content.copy();

        for (String line : tableString) {
            String[] pieces = line
                    .replace("R ", "R")
                    .replace("SR ", "SR")
                    .replace("C ", "C")
                    .replace("S ", "S")
                    .replaceAll("\\|", "")
                    .replaceAll("\\s{2,}", " ")
                    .split("\\s");

            if (pieces.length < 11) {
                log.warn("Cannot filter Forecasts from line: " + line);
                continue;
            }
            JsonObject forecast = content.copy();
            forecast
                    .put("uhrzeit", pieces[0])
                    .put("luft", pieces[1])
                    .put("niederschlag", pieces[2])
                    .put("bewoelkung", pieces[3])
                    .put("wind", pieces[4])
                    .put("innenstadt", new JsonObject()
                            .put("belagstemp", pieces[5])
                            .put("zustand", pieces[6]))
                    .put("nebenstrecke", new JsonObject()
                            .put("belagstemp", pieces[7])
                            .put("zustand", pieces[8]))
                    .put("bruecke", new JsonObject()
                            .put("belag", pieces[9])
                            .put("zustand", pieces[10]));

            result.getJsonArray("forecast").add(forecast);
        }
        return result;

    }

    private JsonObject extractSchema2(List<String> tableString, JsonObject content) {
        JsonObject result = content.copy();
        JsonArray table = result.getJsonArray("forecast");
        String flag1 = ""; // for caching referencing atomic key names
        String flag2 = ""; // for caching referencing key/vale-object key names


        for (int i = 0; i < tableString.size(); i++) {
            List<String> pieces = Arrays.stream(tableString.get(i)
                    .replace("R ", "R")
                    .replace("SR ", "SR")
                    .replace("S ", "S")
                    .replace("C ", "C")
                    .replace("(km/h)", "")
                    .replaceAll("\\s{2,}", " ")
                    .split("\\s"))
                    .filter(item -> !item.isBlank())
                    .collect(Collectors.toList());

            if (pieces.size() > 10) {
                log.warn("Cannot filter Forecasts from tableString.get(i): " + tableString.get(i));
                return new JsonObject();
            }


            // If separated from for loop -> pieces has size of 1
            if (pieces.get(0).equals("mittlere"))
                flag1 = pieces.get(0);

            /**   Table: i = index of attribute; l = index of attribute Forecast  */
            for (int l = 1; l < pieces.size(); l++) { // starting at one, first entry is the key
                while (table.size() < l) table.add(content.copy());

                switch (pieces.get(0)) {
                    // Mit Timestamp ersetzen
                    case "Uhrzeit": {
                        table.getJsonObject(l - 1)
                                .put("uhrzeit", pieces.get(l));
                        break;
                    }
                    /*
                    case "max": {
                        flag1 = "max";
                        table.getJsonObject(l - 1).put("max", pieces.get(l));
                        break;
                    }
                    case "min": {
                        if (!flag1.isEmpty()) {
                            String newlastKeyName = flag1 + "_min";
                            table.getJsonObject(l - 1).put(newlastKeyName, pieces.get(l));
                            if (l + 1 == pieces.size()) flag1 = "";
                        }
                        break;
                    }

                     */
                    case "Bewoelkung": {
                        /*
                        if (flag1.equals("max")) {
                            String newlastKeyName = "Bewoelkung_" + flag1;
                            for (int k = 0; k < pieces.size() - 1; k++) {
                                table.getJsonObject(k)
                                        .put(newlastKeyName, table.getJsonObject(k).getString(flag1))
                                        .put("Bewoelkung", pieces.get(l));
                                table.getJsonObject(k).remove(flag1);
                            }
                            flag1 = "Bewoelkung";
                            break;
                        }
                         */

                        table.getJsonObject(l - 1)
                                .put("bewoelkung", pieces.get(l));
                        break;
                    }

                    case "Niederschlag": {
                        /*
                        if (flag1.equals("max")) {
                            String newlastKeyName = "Niederschlag_" + flag1;
                            for (int k = 0; k < pieces.size() - 1; k++) {
                                table.getJsonObject(k)
                                        .put(newlastKeyName, table.getJsonObject(k).getString(flag1))
                                        .put("Niederschlag", pieces.get(l));
                                table.getJsonObject(k).remove(flag1);
                            }
                            flag1 = "Niederschlag";
                            break;
                        }
                         */
                        table.getJsonObject(l - 1)
                                .put("niederschlag", pieces.get(l));
                        break;
                    }
                    /*
                    case "N-Signal": {
                        table.getJsonObject(l - 1).put("N-Signal", pieces.get(l));
                        break;
                    }
                     */
                    case "Lufttemperatur": {
                        /*
                        if (flag1.equals("max")) {
                            String newlastKeyName = "Lufttemperatur_" + flag1;
                            for (int k = 0; k < pieces.size() - 1; k++) {
                                table.getJsonObject(k)
                                        .put(newlastKeyName, table.getJsonObject(k).getString(flag1))
                                        .put("Lufttemperatur", pieces.get(l));
                                table.getJsonObject(k).remove(flag1);
                            }
                            flag1 = "Lufttemperatur";
                        }
                         */
                        table.getJsonObject(l - 1)
                                .put("lufttemperatur", pieces.get(l));
                        break;
                    }
                    case "Wind": {
                        table.getJsonObject(l - 1)
                                .put("wind", pieces.get(l));
                        break;
                    }
                    /*
                    case "Boeen": {
                        table.getJsonObject(l - 1).put("Boeen", pieces.get(l));
                        break;
                    }
                     */
                    case "Streckentyp:": {
                        flag2 = transformStreetType(pieces.get(1));
                        break;
                    }
                    case "Belagstemp.": {
                        if (flag1.equals("mittlere")) {
                            // Add Straßentyp as Object it not existing
                            if (!table.getJsonObject(l - 1).containsKey(flag2))
                                table.getJsonObject(l - 1)
                                        .put(flag2,
                                                new JsonObject());
                            table
                                    .getJsonObject(l - 1)
                                    .getJsonObject(flag2)
                                    .put("belagstemp", pieces.get(l));
                        }
                        break;
                    }
                    case "Zustand": {
                        if (flag1.equals("mittlere")) {
                            if (!table.getJsonObject(l - 1).containsKey(flag2))
                                table.getJsonObject(l - 1)
                                        .put(flag2,
                                                new JsonObject());
                            table
                                    .getJsonObject(l - 1)
                                    .getJsonObject(flag2)
                                    .put("zustand", pieces.get(l));
                        }
                        break;

                    }

                }
            }
        }

        return result;
    }

    private String transformStreetType(String type) {
        if (type.equals("Strasse")) return "innenstadt";
        else if (type.equals("Schatten")) return "nebenstrecke";
        else if (type.equals("Bruecke")) return "bruecke";
        else {
            log.error("Could not find street type with name {}", type);
            return "";
        }
    }

    private String extractPattern(Pattern pattern, String fromExtract) {
        Matcher m = pattern.matcher(fromExtract);
        while (m.find()) {
            return m.group();
        }
        return "";
    }
}
