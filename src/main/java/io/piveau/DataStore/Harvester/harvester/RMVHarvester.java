package io.piveau.DataStore.Harvester.harvester;

import io.piveau.DataStore.Commons.schema.KafkaKeys;
import io.piveau.DataStore.Commons.schema.KafkaTopics;
import io.piveau.DataStore.Commons.schema.harvester.openData.rmv.Departure;
import io.piveau.DataStore.Commons.schema.harvester.openData.rmv.PrognosisType;
import io.piveau.DataStore.Harvester.util.ApplicationConfig;
import io.piveau.DataStore.Harvester.util.Utils;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.client.predicate.ResponsePredicate;
import io.vertx.kafka.client.producer.KafkaProducer;
import io.vertx.kafka.client.producer.KafkaProducerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

public class RMVHarvester extends AbstractVerticle {

    private static final Logger log = LoggerFactory.getLogger(RMVHarvester.class);

    private WebClient client;

    private String accessId;
    private String region;
    private Integer maxNumberOfStations;
    private long fetchIntervalInSeconds;

    private final AtomicLong queryCount = new AtomicLong();


    @Override
    public void start() {
        mapLocalConfig();
        Map<String, String> kafkaProducerConfig = Utils.getKafkaProducerConfig(config());

        KafkaProducer<String, String> producer = KafkaProducer.create(vertx, kafkaProducerConfig);
        client = WebClient.create(vertx);

        vertx.setPeriodic(fetchIntervalInSeconds * 1000, id -> {
                    log.info("Issued [{}] queries to RMV API", queryCount.get());
                    queryCount.set(0);

                    fetchLocationExtIds().onSuccess(extIds ->
                            extIds.forEach(extId ->
                                    fetchDepartureBoardByExtId(extId)
                                            .onSuccess(departures -> departures.forEach(departure -> {
                                                try {
                                                    String kafkaMessage = Utils.mapFromKeyStructure(departure).encode();

                                                    KafkaProducerRecord<String, String> record =
                                                            KafkaProducerRecord.create(KafkaTopics.OPENDATA_RMV_DEPARTURE.topicName, KafkaKeys.OPENDATA_RMV_DEPARTURE.keyName, kafkaMessage);

                                                    producer.write(record, writeDeparture -> {
                                                        if (writeDeparture.succeeded()) {
                                                            log.debug("Wrote message to Kafka");
                                                        } else {
                                                            log.error("Failed to write message to kafka", writeDeparture.cause());
                                                        }
                                                    });
                                                } catch (IOException e) {
                                                    log.error("Failed to parse departure object", e);
                                                }
                                            }))
                                            .onFailure(failure -> log.error("{}", failure.getMessage(), failure.getCause()))
                            )).onFailure(failure -> log.error("{}", failure.getMessage(), failure.getCause()));
                }
        );
    }

    private Future<List<String>> fetchLocationExtIds() {
        return Future.future(fetchExtIds ->
                client.getAbs("https://www.rmv.de/hapi/location.name")
                        .addQueryParam("input", region)
                        .addQueryParam("accessId", accessId)
                        .addQueryParam("maxNo", maxNumberOfStations.toString())
                        .addQueryParam("format", "json")
                        .expect(ResponsePredicate.SC_OK)
                        .send(fetch -> {
                            queryCount.incrementAndGet();
                            if (fetch.succeeded()) {
                                JsonArray extIds = fetch.result()
                                        .bodyAsJsonObject().getJsonArray("stopLocationOrCoordLocation", new JsonArray());

                                fetchExtIds.complete(extIds.stream()
                                        .map(stopLocation ->
                                                ((JsonObject) stopLocation).getJsonObject("StopLocation"))
                                        .filter(Objects::nonNull)
                                        .map(stopLocation -> stopLocation.getString("extId"))
                                        .collect(Collectors.toList()));
                            } else {
                                fetchExtIds.fail(fetch.cause());
                            }
                        }));
    }

    private Future<List<Departure>> fetchDepartureBoardByExtId(String locationExtId) {
        return Future.future(fetchDepartures ->
                client.getAbs("https://www.rmv.de/hapi/departureBoard")
                        .addQueryParam("extId", locationExtId)
                        .addQueryParam("accessId", accessId)
                        .addQueryParam("format", "json")
                        .expect(ResponsePredicate.SC_OK)
                        .send(fetch -> {
                            queryCount.incrementAndGet();
                            if (fetch.succeeded()) {
                                JsonArray departures = fetch.result()
                                        .bodyAsJsonObject().getJsonArray("Departure", new JsonArray());

                                fetchDepartures.complete(departures
                                        .stream()
                                        .filter(Objects::nonNull)
                                        .map(departure -> (JsonObject) departure)
                                        .map(this::mapToDeparture)
                                        .collect(Collectors.toList()));
                            } else {
                                fetchDepartures.fail(fetch.cause());
                            }
                        }));
    }

    private Departure mapToDeparture(JsonObject response) {
        Departure departure = new Departure();
        departure.setTimestamp(Timestamp.valueOf(LocalDateTime.now()));

        departure.setName(response.getString("name"));
        departure.setType(response.getString("type"));
        departure.setStop(response.getString("stop"));
        departure.setStopId(response.getString("stopid"));
        departure.setStopExtId(response.getString("stopExtId"));

        if (response.containsKey("JourneyDetailRef"))
            departure.setJourneyDetailRef(response.getJsonObject("JourneyDetailRef").getString("ref"));

        if (response.containsKey("prognosisType"))
            departure.setPrognosisType(PrognosisType.valueOf(response.getString("prognosisType")));

        if (response.containsKey("timeZone"))
            departure.setTimeZone(response.getString("timeZone"));

        if (response.containsKey("rtTimeZone"))
            departure.setRtTimeZone(response.getString("rtTimeZone"));

        if (response.containsKey("time"))
            departure.setTime(response.getString("time"));

        if (response.containsKey("rtTime"))
            departure.setRtTime(response.getString("rtTime"));

        if (response.containsKey("date"))
            departure.setDate(response.getString("date"));

        if (response.containsKey("rtDate"))
            departure.setRtDate(response.getString("rtDate"));

        if (response.containsKey("timeAtArrival"))
            departure.setTimeAtArrival(response.getString("timeAtArrival"));

        if (response.containsKey("rtTimeAtArrival"))
            departure.setRtTimeAtArrival(response.getString("rtTimeAtArrival"));

        if (response.containsKey("dateAtArrival"))
            departure.setDateAtArrival(response.getString("dateAtArrival"));

        if (response.containsKey("rtDateAtArrival"))
            departure.setRtDateAtArrival(response.getString("rtDateAtArrival"));

        departure.setTrainNumber(response.getString("trainNumber"));
        departure.setTrainCategory(response.getString("trainCategory"));
        departure.setTrack(response.getString("track"));
        departure.setRtTrack(response.getString("rtTrack"));
        departure.setDirection(response.getString("direction"));

        departure.setReachable(response.getBoolean("reachable"));
        departure.setCancelled(response.getBoolean("cancelled"));
        departure.setPartCancelled(response.getBoolean("partCancelled"));
        departure.setFastest(response.getBoolean("fastest"));

        return departure;
    }

    private void mapLocalConfig() {
        JsonObject harvesterConfig = config().getJsonObject(ApplicationConfig.ENV_RMV_CONFIG);

        this.fetchIntervalInSeconds = harvesterConfig.getLong("fetchIntervalInSeconds", ApplicationConfig.DEFAULT_FETCH_INTERVAL_IN_SECONDS);
        this.region = harvesterConfig.getString("region", "Rüsselsheim");
        this.maxNumberOfStations = harvesterConfig.getInteger("maxNumberOfStations", 1000);
        this.accessId = harvesterConfig.getString("accessId");
    }

    @Override
    public void stop() {
        client.close();
    }
}