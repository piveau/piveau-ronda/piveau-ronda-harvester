package io.piveau.DataStore.Harvester.harvester;

import io.piveau.DataStore.Commons.schema.KafkaKeys;
import io.piveau.DataStore.Commons.schema.KafkaTopics;
import io.piveau.DataStore.Harvester.util.ApplicationConfig;
import io.piveau.DataStore.Harvester.util.Utils;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.HttpRequest;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;
import io.vertx.kafka.client.producer.KafkaProducer;
import io.vertx.kafka.client.producer.KafkaProducerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * A Harvester that can Harvest the OpenWeatherMap API based on a given cityId.
 * CityId and the API Key must be specified in the config file.
 * The harvested data will be forwarded to the INBOUND_OPENWEATHERMAP specified in the Commons module.
 */
public class OpenWeatherMapHarvester extends AbstractVerticle {
    private static final Logger log = LoggerFactory.getLogger(OpenWeatherMapHarvester.class);

    private KafkaProducer<String, String> producer;
    private WebClient client;

    private String url;
    private String appId;
    private String cityId;
    private Long fetchIntervalInSeconds;
    private Long fetchNewDataId;

    @Override
    public void start() {

        mapLocalConfig();
        Map<String, String> kafkaProducerConfig = Utils.getKafkaProducerConfig(config());

        producer = KafkaProducer.create(vertx, kafkaProducerConfig);
        client = WebClient.create(vertx);

        fetchNewDataId = vertx.setPeriodic(fetchIntervalInSeconds * 1000, id -> {
            log.debug("Fetching Data");
            // Create Request
            HttpRequest<Buffer> request = client
                    .getAbs(url)
                    .addQueryParam("id", cityId)
                    .addQueryParam("appid", appId);

            // Send Request
            request.send(ar -> {
                if (ar.succeeded()) {

                    // Parse Result
                    try {
                        HttpResponse<Buffer> response = ar.result();
                        JsonObject jsonResponse = response.bodyAsJsonObject().put("timestamp", new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZZZZ").format(new Date()));
                        String value = Utils.mapToKeyStructure(jsonResponse.toString(), KafkaKeys.INBOUND_OPENWEATHERMAP);

                        // Write to Kafka
                        KafkaProducerRecord<String, String> record =
                                KafkaProducerRecord.create(KafkaTopics.INBOUND_OPENWEATHERMAP.topicName, KafkaKeys.INBOUND_OPENWEATHERMAP.keyName, value);
                        producer.write(record, written -> {
                            if (written.succeeded()) {
                                log.debug("Written message to Kafka ");
                            } else {
                                log.error("Could not write message to Kafka", written.cause());
                            }
                        });
                    } catch (IOException e) {
                        log.error("Could not write message to Kafka", e);
                    }
                } else {
                    log.warn("Failed to fetch data", ar.cause());
                }
            });
        });
    }

    public void stop() {
        producer.close();
        client.close();

        // Cancel Fetching Weather Data
        if (fetchNewDataId != null) {
            vertx.cancelTimer(fetchNewDataId);
        }
    }

    private void mapLocalConfig() {
        JsonObject harvesterConfig = config().getJsonObject(ApplicationConfig.ENV_OPEN_WEATHER_MAP_CONFIG);

        this.fetchIntervalInSeconds = harvesterConfig.getLong("fetchIntervalInSeconds");
        this.url = harvesterConfig.getString("url");
        this.cityId = harvesterConfig.getString("cityId");
        this.appId = harvesterConfig.getString("appId");
    }
}
