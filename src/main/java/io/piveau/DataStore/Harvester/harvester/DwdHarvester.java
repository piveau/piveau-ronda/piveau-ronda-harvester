package io.piveau.DataStore.Harvester.harvester;

import io.piveau.DataStore.Commons.schema.KafkaKeys;
import io.piveau.DataStore.Commons.schema.KafkaTopics;
import io.piveau.DataStore.Harvester.util.ApplicationConfig;
import io.piveau.DataStore.Harvester.util.Utils;
import io.piveau.DataStore.Harvester.util.DwdParser;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.client.predicate.ResponsePredicate;
import io.vertx.kafka.client.producer.KafkaProducer;
import io.vertx.kafka.client.producer.KafkaProducerRecord;
import io.vertx.kafka.client.producer.RecordMetadata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static io.piveau.DataStore.Commons.schema.KafkaKeys.OPENDATA_DWD_FORECAST;

public class DwdHarvester extends AbstractVerticle {

    private static final Logger log = LoggerFactory.getLogger(DwdHarvester.class);

    private String host;
    private String path;

    private Integer downloadWindowHours;
    private List<String> cityIds;
    private List<Integer> hours;
    private Long fetchIntervalInSeconds;
    private Long periodicJobId;

    private final Map<String, LocalDateTime> lastCrawled = new HashMap<>();

    private WebClient webClient;

    @Override
    public void start() {
        webClient = WebClient.wrap(vertx.createHttpClient());

        mapLocalConfig();
        Map<String, String> kafkaProducerConfig = Utils.getKafkaProducerConfig(config());

        KafkaProducer<String, String> producer = KafkaProducer.create(vertx, kafkaProducerConfig);

        periodicJobId = vertx.setPeriodic(fetchIntervalInSeconds * 1000, handler -> {
            for (String cityId : cityIds) {
                LocalDateTime next = getNextCrawlTime(cityId);
                log.info("Crawling cityId: {} for time {}", cityId, next);
                download(cityId, next.getDayOfMonth(), next.getHour())
                        .onFailure(error -> {
                            if (next.plusHours(downloadWindowHours).isBefore(LocalDateTime.now())) {
                                log.error("Could not download CityId: {} time: {} in Frame of: {} hours. Time is skipped",
                                        cityId, next.toString(), downloadWindowHours, error);
                                this.updateLastCrawledTime(cityId, next);
                            }
                        })
                        .flatMap(rawForecast -> transformForecast(rawForecast, cityId))
                        .map(parsedForecast -> getKafkaProducerRecord(cityId, parsedForecast.toString()))
                        .flatMap(kafkaRecord -> {
                                        Promise<RecordMetadata> result = Promise.promise();
                                        producer.send(kafkaRecord, result);
                                        return result.future();
                                })
                        .onSuccess(success -> {
                            log.info("Successfully written to Kafka for {} with time {}", cityId, next);
                            updateLastCrawledTime(cityId, next);
                        }
                        )
                    .onFailure(error -> log.error("Could not parse or send Forecast for {} with time {} to kafka", cityId, next, error))

                ;
            }
        });
    }

    @Override
    public void stop() throws Exception {
        super.stop();
        vertx.cancelTimer(periodicJobId);
    }

    private Future<String> download(String cityId, Integer dayOfMonth, Integer hour) {
        return Future.future(download -> {
            String url = getUri(cityId, dayOfMonth, hour);

            webClient.getAbs(url)
                    .expect(ResponsePredicate.SC_OK)
                    .send(request -> {
                        if (request.succeeded()) {
                            download.complete(request.result().bodyAsString());
                        } else {
                            download.fail(request.cause());
                        }
                    });
        });
    }


    private Future<JsonObject> transformForecast(String toTransform, String cityId){
        Promise<JsonObject> result = Promise.promise();
        DwdParser parser = new DwdParser(toTransform);
        JsonObject forecast = parser.extractTable();

        forecast.put("location", idToCity(cityId));

        try {
            JsonObject validForecast = new JsonObject(Utils.mapToKeyStructure(forecast.toString(), OPENDATA_DWD_FORECAST));
            result.complete(validForecast);
        } catch (IOException e) {
            result.fail("Could not map result to Key Structure");
        }

        return result.future();
    }

    private void mapLocalConfig() {
        JsonObject harvesterConfig = config().getJsonObject(ApplicationConfig.ENV_DWD_CONFIG);

        this.host = harvesterConfig.getString("host");
        this.path = harvesterConfig.getString("path");
        this.fetchIntervalInSeconds = harvesterConfig.getLong("fetchIntervalInSeconds", ApplicationConfig.DEFAULT_FETCH_INTERVAL_IN_SECONDS);
        this.downloadWindowHours = harvesterConfig.getInteger("downloadWindowHours", 4);
        this.cityIds = (List<String>) harvesterConfig.getJsonArray("cityIds").getList();
        this.hours = (List<Integer>) harvesterConfig.getJsonArray("hours").getList()
                .stream().sorted().collect(Collectors.toList());
    }


    private LocalDateTime getNextCrawlTime(String cityId) {
        LocalDateTime last = lastCrawled.getOrDefault(cityId, LocalDateTime.now().with(LocalTime.of(0, 0, 0, 0)));
        Integer lastHour = last.getHour();

        Integer nextHour = hours.get(0);
        for (Integer hour : hours) {
            if (lastHour < hour) {
                nextHour = hour;
                break;
            }
        }

        LocalDateTime next = last;
        if (nextHour < lastHour) {
            next = next.plusDays(1);
        }
        next = next.with(LocalTime.of(nextHour, 0));

        return next;
    }

    private String hourToTimecode(Integer hour) {
        return hour.toString().length() < 2 ? "0" + hour.toString() : hour.toString();
    }

    private String getUri(String cityId, Integer dayOfMonth, Integer hour) {
        String timecode = hourToTimecode(hour);
        String daycode = dayOfMonth.toString().length() < 2 ? "0" + dayOfMonth.toString() : dayOfMonth.toString();
        return "https://" + host + path + cityId + "_" + daycode + timecode + "00";
    }

    private void updateLastCrawledTime(String cityId, LocalDateTime localDateTime) {
        this.lastCrawled.put(cityId, localDateTime);
    }

    private KafkaProducerRecord getKafkaProducerRecord(String cityId, String jsonRecord) {
        String topicName = "";
        String keyName = KafkaKeys.OPENDATA_DWD_FORECAST.keyName;
        if (cityId.equals("FDBJ06_FXXX")) {
            topicName = KafkaTopics.OPENDATA_DWD_FORECAST_FRANKFURT.topicName;
        } else if (cityId.equals("FDBJ07_MXXX")) {
            topicName = KafkaTopics.OPENDATA_DWD_FORECAST_MAINZ.topicName;
        } else if (cityId.equals("FDBJ06_DXXX")) {
            topicName = KafkaTopics.OPENDATA_DWD_FORECAST_DARMSTADT.topicName;
        } else if (cityId.equals("FDAT23_BJOF")) {
            topicName = KafkaTopics.OPENDATA_DWD_FORECAST_FLUGHAFEN_FRANKFURT.topicName;
        } else if (cityId.equals("FDBJ06_WXXX")) {
            topicName = KafkaTopics.OPENDATA_DWD_FORECAST_WIESBADEN.topicName;
        }
        if (topicName.isEmpty())
            throw new NullPointerException("Could not create KafkaProducerRecord: City has no Topic.");
        return KafkaProducerRecord.create(topicName, keyName, jsonRecord);
    }

    private String idToCity(String cityId) {
        if (cityId.equals("FDBJ06_FXXX")) return "Frankfurt";
        else if (cityId.equals("FDBJ07_MXXX")) return "Mainz";
        else if (cityId.equals("FDBJ06_DXXX")) return "Darmstadt";
        else if (cityId.equals("FDAT23_BJOF")) return "Flughafen Frankfurt";
        else if (cityId.equals("FDBJ06_WXXX")) return "Wiesbaden";
        throw new NullPointerException("Unkowkn cityId: Could not map id to to City");
    }
}
