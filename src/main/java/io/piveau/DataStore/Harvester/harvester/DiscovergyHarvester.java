package io.piveau.DataStore.Harvester.harvester;

import io.piveau.DataStore.Commons.schema.KafkaKeys;
import io.piveau.DataStore.Commons.schema.KafkaTopics;
import io.piveau.DataStore.Harvester.util.ApplicationConfig;
import io.piveau.DataStore.Harvester.util.Utils;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;
import io.vertx.kafka.client.common.PartitionInfo;
import io.vertx.kafka.client.common.TopicPartition;
import io.vertx.kafka.client.consumer.KafkaConsumer;
import io.vertx.kafka.client.consumer.KafkaConsumerRecord;
import io.vertx.kafka.client.consumer.KafkaConsumerRecords;
import io.vertx.kafka.client.producer.KafkaProducer;
import io.vertx.kafka.client.producer.KafkaProducerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

public class DiscovergyHarvester extends AbstractVerticle {
    private static final Logger log = LoggerFactory.getLogger(DiscovergyHarvester.class);

    private WebClient webClient;
    private KafkaProducer<String, String> producer;

    private Map<String, String> kafkaConsumerConfig;

    private String host;
    private String user;
    private String password;
    private Long fetchIntervalInSeconds;
    private Long earliestLookupTimestamp;
    private Long lastReadingTime;
    private List<String> meterIds;
    private Map<String, JsonObject> locations;


    @Override
    public void start() {
        locations = new HashMap<>();
        webClient = WebClient.wrap(vertx.createHttpClient());

        mapLocalConfig();
        kafkaConsumerConfig = Utils.getKafkaConsumerConfig(config());
        kafkaConsumerConfig.put("group.id", DiscovergyHarvester.class.getCanonicalName());

        Map<String, String> kafkaProducerConfig = Utils.getKafkaProducerConfig(config());

        this.producer = KafkaProducer.create(vertx, kafkaProducerConfig);
        this.meterIds = new ArrayList<>();

        getMeterIdsAndLocations()
                .flatMap(meterIds -> this.setLastReadingTime().map(aVoid -> meterIds))
                .map(meterIds -> {
                    log.info("Last Reading times updated");
                    return meterIds;
                })
                .onSuccess(meterIds -> this.meterIds = meterIds)
                .onFailure(error -> log.error("Could not fetch meterIds", error));

        vertx.setPeriodic(fetchIntervalInSeconds * 1000, handler -> {
            saveNewReadings()
                    .onSuccess(amount -> {
                        if (amount > 0)
                            log.debug("Transferred new Readings [{}] to Kafka.", amount);
                    })
                    .onFailure(error -> log.error("Could not transfer new Readings to Kafka", error));
        });
    }

    private Future<List<JsonObject>> getReadings(List<String> meterIds, Long from, Long to) {
        List<String> restMeterIds = new ArrayList<>(meterIds);
        if (restMeterIds.isEmpty())
            return Future.succeededFuture(new ArrayList<>());

        return getReadings(restMeterIds.remove(0), from, to)
                .flatMap(firstMeter -> getReadings(restMeterIds, from, to)
                        .map(rest -> {
                                    rest.addAll(firstMeter);
                                    return rest;
                                }
                        )
                );
    }

    private Future<List<JsonObject>> getReadings(String meterId, Long from, Long to) {
        Promise<HttpResponse<Buffer>> request = Promise.promise();

        webClient.get(443, host, "/public/v1/readings")
                .addQueryParam("meterId", meterId)
                .addQueryParam("from", from.toString())
                .addQueryParam("to", to.toString())
                .basicAuthentication(user, password)
                .ssl(true)
                .send(request);

        return request
                .future()
                .map(HttpResponse::bodyAsJsonArray)
                .map(JsonArray::stream)
                .map(stream -> stream.map(obj -> ((JsonObject) obj)))
                .map(stream ->
                        stream.peek(reading -> {
                            reading.put("location", getLocationOfMeter(meterId));
                            reading.put("timestamp", new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZZZZ").format(new Date(reading.getLong("time"))));
                            reading.remove("time");
                        })
                )
                .map(stream -> stream.collect(Collectors.toList()));
    }

    private Future<Integer> saveNewReadings() {
        Promise<Integer> result = Promise.promise();

        Long toReadingTime = Math.max(lastReadingTime + 1, Math.min(System.currentTimeMillis() - 120000, lastReadingTime + 86400000));

        this.getReadings(this.meterIds, lastReadingTime + 1, toReadingTime)
                .map(Collection::stream)
                .map(readings -> readings
                        .sorted(Comparator.comparing(json -> {
                            long time = -1L;
                            try {
                                time = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZZZZ").parse(json.getString("timestamp")).getTime();
                            } catch (ParseException e) {
                                log.error("Failed to format date", e);
                            }
                            return time;
                        }))
                        .map(reading -> KafkaProducerRecord.create(KafkaTopics.INBOUND_DISCOVERGY.topicName,
                                KafkaKeys.INBOUND_DISCOVERGY_METER_ALL.keyName,
                                reading.toString())
                        )
                        .map(kafkaRecord -> {
                            Promise<Void> written = Promise.promise();
                            this.producer.write(kafkaRecord, written);
                            return (Future) written.future();

                        }).collect(Collectors.toList())
                )
                .map(CompositeFuture::all)
                .flatMap(compositeFuture -> compositeFuture.map(compositeFuture.size()))
                .map(future -> {
                    this.lastReadingTime = toReadingTime;
                    return future;
                })
                .onSuccess(result::complete)
                .onFailure(result::fail);

        return result.future();
    }

    private Future<Void> setLastReadingTime() {
        Promise<Void> result = Promise.promise();
        KafkaConsumer<String, String> consumer = KafkaConsumer.create(vertx, kafkaConsumerConfig);
        Promise<List<PartitionInfo>> partitionsInfoPromise = Promise.promise();
        consumer.partitionsFor(KafkaTopics.INBOUND_DISCOVERGY.topicName, partitionsInfoPromise);

        this.lastReadingTime = this.earliestLookupTimestamp;

        partitionsInfoPromise
                .future()
                .map(partitionInfos -> partitionInfos.stream()
                        .map(info -> new TopicPartition().setPartition(info.getPartition()).setTopic(info.getTopic()))
                        .collect(Collectors.toSet())
                )
                .flatMap(topicPartitions -> {
                    Promise<Map<TopicPartition, Long>> endoffsetsPromise = Promise.promise();
                    consumer.endOffsets(topicPartitions, endoffsetsPromise);

                    return endoffsetsPromise.future();
                })
                .flatMap(endoffsets -> {
                    Promise<Void> done = Promise.promise();
                    consumer.assign(endoffsets.keySet(), done);

                    return done.future().map(aVoid -> endoffsets);
                })
                .flatMap(endoffsets -> {
                    List<Future> setOffsets = endoffsets.entrySet()
                            .stream()
                            .map(entry -> {
                                        TopicPartition partition = entry.getKey();
                                        Long endoffset = entry.getValue();
                                        Promise<Void> done = Promise.promise();
                                        if (endoffset > 0) {
                                            consumer.seek(partition, endoffset - 1, done);
                                        } else {
                                            done.complete();
                                        }

                                        return (Future) done.future();
                                    }
                            ).collect(Collectors.toList());

                    return CompositeFuture.all(setOffsets).flatMap(done -> Future.succeededFuture());
                }).flatMap(aVoid -> {
                    Promise<KafkaConsumerRecords<String, String>> pollResult = Promise.promise();
                    consumer.poll(100, pollResult);
                    return pollResult.future();
                }
        ).flatMap(kafkaRecords -> {
            for (int i = 0; i < kafkaRecords.size(); i++) {
                KafkaConsumerRecord<String, String> record = kafkaRecords.recordAt(i);
                Long time = -1L;
                try {
                    String timestamp = new JsonObject(record.value()).getString("timestamp");
                    time = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZZZZ").parse(timestamp).getTime();
                } catch (ParseException e) {
                    log.error("Could not parse Time from Timestamp");
                }
                if (time > this.lastReadingTime)
                    this.lastReadingTime = time;
            }
            return Future.succeededFuture();
        })
                .onSuccess(aVoid -> result.complete())
                .onFailure(error -> {
                    log.error("Could not load all Last reading Times", error);
                    result.complete();
                });

        return result.future();
    }

    private Future<List<String>> getMeterIdsAndLocations() {
        Promise<HttpResponse<Buffer>> request = Promise.promise();

        webClient.get(443, host, "/public/v1/meters")
                .basicAuthentication(user, password)
                .ssl(true)
                .send(request);

        return request.future()
                .map(response -> response
                        .bodyAsJsonArray()
                        .stream()
                        .map(jsonObject -> ((JsonObject) jsonObject))
                        .map(jsonObject -> {
                            String meterId = jsonObject.getString("meterId");
                            this.locations.put(meterId, jsonObject.getJsonObject("location"));
                            return meterId;
                        })
                        .collect(Collectors.toList()));
    }

    private void mapLocalConfig() {
        JsonObject harvesterConfig = config().getJsonObject(ApplicationConfig.ENV_DISCOVERGY_CONFIG);

        this.host = harvesterConfig.getString("host");
        this.user = harvesterConfig.getString("user");
        this.password = harvesterConfig.getString("password");
        this.fetchIntervalInSeconds = harvesterConfig.getLong("fetchIntervalInSeconds", ApplicationConfig.DEFAULT_FETCH_INTERVAL_IN_SECONDS);
        this.earliestLookupTimestamp = harvesterConfig.getLong("earliestLookupTimestamp", 1577833200000L);
    }


    private JsonObject getLocationOfMeter(String meterId) {
        return this.locations.getOrDefault(meterId, new JsonObject());
    }
}
