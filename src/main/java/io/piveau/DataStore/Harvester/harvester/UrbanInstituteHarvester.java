package io.piveau.DataStore.Harvester.harvester;

import io.piveau.DataStore.Commons.schema.KafkaKeys;
import io.piveau.DataStore.Commons.schema.KafkaTopics;
import io.piveau.DataStore.Commons.schema.base.Key;
import io.piveau.DataStore.Harvester.util.ApplicationConfig;
import io.piveau.DataStore.Harvester.util.Utils;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.WebSocketConnectOptions;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.client.predicate.ResponsePredicate;
import io.vertx.kafka.client.producer.KafkaProducer;
import io.vertx.kafka.client.producer.KafkaProducerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class UrbanInstituteHarvester extends AbstractVerticle {
    private static final Logger log = LoggerFactory.getLogger(UrbanInstituteHarvester.class);

    private WebClient webClient;
    private HttpClient client;
    private KafkaProducer<String, String> producer;

    private String host;
    private Integer port;
    private String user;
    private String password;
    private Integer reconnectMS;
    private List<String> statementIds;


    // Called when verticle is deployed
    public void start() {
        client = vertx.createHttpClient();
        webClient = WebClient.wrap(vertx.createHttpClient());
        mapLocalConfig();
        Map<String, String> kafkaProducerConfig = Utils.getKafkaProducerConfig(config());

        // Websocket Client Urban Institute --> KAFKA
        producer = KafkaProducer.create(vertx, kafkaProducerConfig);

        this.webSocketConnections().forEach(websocketConnectionFuture ->
                websocketConnectionFuture
                        .onSuccess(wsc -> {
                            if (wsc != null)
                                this.startWebsocket(wsc);
                        })
                        .onFailure(failure -> log.error("Failed to connect websockets", failure))
        );
    }

    private Future<Optional<String>> getOutboundAddress(String statementId) {
        Promise<Optional<String>> outBoundAddress = Promise.promise();
        log.debug("Handling Statement ID: " + statementId);

        //Create Request Option

        // Send Request
        Promise<HttpResponse<Buffer>> rawResult = Promise.promise();
        webClient.get(port, host, "/UrbanPulseManagement/api/statements/" + statementId + "/update-listeners")
                .expect(ResponsePredicate.SC_OK)
                .putHeader("Authorization", Utils.BasicAuthEncode(user, password))
                .ssl(true)
                .send(rawResult);

        //Parse Result
        rawResult.future()
                .map(HttpResponse::bodyAsJsonObject)
                .map(jsonObject -> jsonObject.getJsonArray("listeners"))
                .map(jsonArray -> IntStream
                        .range(0, jsonArray.size())
                        .mapToObj(jsonArray::getJsonObject)
                        .map(jsonObject -> jsonObject.getString("target"))
                        .filter(targetString -> targetString.startsWith("wss://") &&
                                targetString.contains("/OutboundInterfaces/outbound/")
                        )
                        .map(targetString -> targetString.replace("//local.", "//ebc."))
                        .findFirst()
                )
                .setHandler(outBoundAddress)
                .onFailure(failure -> log.error("Failed to parse result", failure));

        return outBoundAddress.future();
    }

    private WebSocketConnectOptions createWebsocketOption(String address) {
        URI websocket = URI.create(address);

        return new WebSocketConnectOptions()
                .setHost(websocket.getHost())
                .setPort(websocket.getPort() > 0 ? websocket.getPort() : 443)
                .setSsl(true)
                .addHeader("Authorization", Utils.BasicAuthEncode(user, password))
                .setURI(websocket.getPath());
    }

    private List<Future<WebSocketConnectOptions>> webSocketConnections() {
        return this.statementIds.stream()
                .map(this::getOutboundAddress)
                .map(outBoundFuture ->
                        outBoundFuture.map(v -> v.map(this::createWebsocketOption).orElse(null)))
                .collect(Collectors.toList());
    }

    private void startWebsocket(WebSocketConnectOptions connectionOptions) {
        log.debug("Trying websocket connection on: " + connectionOptions.getURI());

        client.webSocket(connectionOptions, websocket -> {
            if (websocket.succeeded()) {
                // Check every 10 minutes if messages arrived
                AtomicBoolean messageReceived = new AtomicBoolean(false);
                AtomicReference<Long> periodicCheck = new AtomicReference<>();

                vertx.setPeriodic(6000000, periodic -> {
                    periodicCheck.set(periodic);
                    if (!messageReceived.getAndSet(false)) {
                        log.info("Websocket [{}] is unhealthy, restarting Websocket", connectionOptions.getURI());
                        websocket.result().close();
                        Long pc = periodicCheck.getAndSet(null);
                        if (pc != null) {
                            vertx.cancelTimer(pc);
                        }
                    } else {
                        log.debug("Websocket [{}] is healthy", connectionOptions.getURI());
                    }
                });

                websocket.result().handler(data -> {
                    messageReceived.set(true);
                    this.handleMessage(data);
                });

                AtomicBoolean restarted = new AtomicBoolean(false);

                websocket.result().exceptionHandler(handler -> handleWebSocketResult(connectionOptions, periodicCheck, restarted));
                websocket.result().endHandler(handler -> handleWebSocketResult(connectionOptions, periodicCheck, restarted));
                websocket.result().drainHandler(handler -> handleWebSocketResult(connectionOptions, periodicCheck, restarted));
                websocket.result().closeHandler(handler -> handleWebSocketResult(connectionOptions, periodicCheck, restarted));
            } else {
                vertx.setTimer(reconnectMS * 2, aVoid -> startWebsocket(connectionOptions));
                log.error("Could not create Websocket for: " + connectionOptions.getHost() + connectionOptions.getURI());
            }
        });
    }

    private void handleWebSocketResult(WebSocketConnectOptions connectionOptions, AtomicReference<Long> periodicCheck, AtomicBoolean restarted) {
        Long pc = periodicCheck.getAndSet(null);
        if (pc != null) {
            vertx.cancelTimer(pc);
        }
        if (!restarted.getAndSet(true)) {
            log.info("Restarting Websocket: " + connectionOptions.getURI());
            vertx.setTimer(reconnectMS, aVoid2 -> this.startWebsocket(connectionOptions));
        } else {
            log.debug("already restarted");
        }
    }

    // Optional - called when verticle is undeployed
    public void stop() {
        client.close();
        producer.close();
    }

    private void mapLocalConfig() {
        JsonObject harvesterConfig = config().getJsonObject(ApplicationConfig.ENV_URBAN_INSTITUTE_CONFIG);

        this.host = harvesterConfig.getString("host");
        this.port = harvesterConfig.getInteger("port");
        this.user = harvesterConfig.getString("user");
        this.password = harvesterConfig.getString("password");
        this.reconnectMS = harvesterConfig.getInteger("reconnectMS");
        this.statementIds = (List<String>) harvesterConfig.getJsonArray("statementIds").getList();
    }

    private void handleMessage(Buffer data) {
        JsonObject jsonData = data.toJsonObject();
        Key<?> key = UrbanInstituteHarvester.statementNameToKey(jsonData.getString("statementName", "<NOT FOUND>"));

        if (key != null) {
            try {
                String value = Utils.mapToKeyStructure(jsonData.encode(), key);
                KafkaProducerRecord<String, String> record =
                        KafkaProducerRecord.create(KafkaTopics.INBOUND_URBANINSTITUTE.topicName, key.keyName, value);
                Promise<Void> written = Promise.promise();
                producer.write(record, written);
                written.future().onSuccess(aVoid -> log.debug("Message written"));
                written.future().onFailure(error -> log.error("Could not write message to Kafka", error));
            } catch (Exception e) {
                log.error("Message might have wrong format:" + data, e);
            }
        }
    }

    private static Key<?> statementNameToKey(String statementName) {
        switch(statementName){
            case "DiscovergyEventTypeStatement":
                return KafkaKeys.INBOUND_URBANINSTITUTE_DISCOVERGY;
            case "OpenWeatherMapEventTypeEventTypeStatement":
                return KafkaKeys.INBOUND_URBANINSTITUTE_OPENWEATHER;
            default:
                log.error("StatementName [{}] is unknown", statementName);
                return null;
        }
    }
}
