package io.piveau.DataStore.Harvester.harvester;

import com.github.javafaker.Faker;
import io.piveau.DataStore.Commons.schema.KafkaKeys;
import io.piveau.DataStore.Commons.schema.KafkaTopics;
import io.piveau.DataStore.Harvester.util.ApplicationConfig;
import io.piveau.DataStore.Harvester.util.Utils;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.json.JsonObject;
import io.vertx.kafka.client.producer.KafkaProducer;
import io.vertx.kafka.client.producer.KafkaProducerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.UUID;

/**
 * A Test Harvester producing fake messages in a given time interval.
 * The messages will be forwarded to the INBOUND_TESTAPI topic specified in the Commons module.
 */
public class TestHarvester extends AbstractVerticle {
    private static final Logger log = LoggerFactory.getLogger(TestHarvester.class);

    private KafkaProducer<String, String> producer;

    private Long fetchIntervalInSeconds;
    private Faker faker;
    private Long fetchNewDataId;

    @Override
    public void start() {
        mapLocalConfig();
        Map<String, String> kafkaProducerConfig = Utils.getKafkaProducerConfig(config());

        producer = KafkaProducer.create(vertx, kafkaProducerConfig);
        faker = new Faker();

        fetchNewDataId = vertx.setPeriodic(fetchIntervalInSeconds * 1000, id -> {
            // Create Request
            JsonObject message = new JsonObject()
                    .put("id", UUID.randomUUID().hashCode())
                    .put("userId", UUID.randomUUID().hashCode())
                    .put("title", faker.lorem().word())
                    .put("firstname", faker.name().firstName())
                    .put("lastname", faker.name().lastName())
                    .put("body", faker.lorem().paragraph())
                    .put("timestamp", LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));

            // Write to Kafka
            KafkaProducerRecord<String, String> record =
                    KafkaProducerRecord.create(KafkaTopics.INBOUND_TESTAPI.topicName, KafkaKeys.INBOUND_TESTAPI_KEY.keyName, message.encode());

            producer.write(record, write -> {
                if (write.succeeded()) {
                    log.debug("Wrote message to Kafka");
                } else {
                    log.error("Could not write message to Kafka", write.cause());
                }
            });
        });
    }

    public void stop() {
        producer.close();

        // Cancel Fetching Weather Data
        if (fetchNewDataId != null) {
            vertx.cancelTimer(fetchNewDataId);
        }
    }

    private void mapLocalConfig() {
        JsonObject harvesterConfig = config().getJsonObject(ApplicationConfig.ENV_TEST_CONFIG);

        this.fetchIntervalInSeconds = harvesterConfig.getLong("fetchIntervalInSeconds", ApplicationConfig.DEFAULT_FETCH_INTERVAL_IN_SECONDS);
    }
}
