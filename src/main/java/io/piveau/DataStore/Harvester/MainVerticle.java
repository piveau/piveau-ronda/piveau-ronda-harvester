package io.piveau.DataStore.Harvester;

import io.piveau.DataStore.Harvester.harvester.*;
import io.vertx.config.ConfigRetriever;
import io.vertx.config.ConfigRetrieverOptions;
import io.vertx.config.ConfigStoreOptions;
import io.vertx.core.*;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static io.piveau.DataStore.Harvester.util.ApplicationConfig.*;


public class MainVerticle extends AbstractVerticle {

    private static final Logger log = LoggerFactory.getLogger(MainVerticle.class);

    private JsonObject config;

    @Override
    public void start(Promise<Void> startPromise) {
        // startup is only successful if no step failed
        loadConfig()
                .compose(handler -> bootstrapVerticles())
                .onSuccess(success -> startPromise.complete())
                .onFailure(startPromise::fail);
    }

    private Future<Void> loadConfig() {
        return Future.future(loadConfig -> {
            ConfigRetrieverOptions options = new ConfigRetrieverOptions()
                    .addStore(new ConfigStoreOptions()
                            .setType("file")
                            .setFormat("json")
                            .setConfig(new JsonObject().put("path", PATH_JSON_CONFIG))
                            .setOptional(true)
                    )
                    .addStore(new ConfigStoreOptions()
                            .setType("env")
                    );

            ConfigRetriever.create(vertx, options).getConfig(handler -> {
                if (handler.succeeded()) {
                    config = handler.result();
                    log.debug(config.encodePrettily());

                    if (config.containsKey(ENV_KAFKA_CONFIG)) {
                        loadConfig.complete();
                    } else {
                        loadConfig.fail("Kafka config is missing");
                    }
                } else {
                    loadConfig.fail("Failed to load config: " + handler.cause());
                }
            });
        });
    }

    /**
     * Starts all Harvester Verticles
     *
     * @return Future succeeds when all Verticles started successfully and fails otherwise
     */
    private CompositeFuture bootstrapVerticles() {

        DeploymentOptions options = new DeploymentOptions()
                .setConfig(config)
                .setWorker(true);

        List<Future<Void>> deployments = Arrays.asList(
                startVerticleIfApplicable(options, ENV_URBAN_INSTITUTE_CONFIG, UrbanInstituteHarvester.class),
                startVerticleIfApplicable(options, ENV_OPEN_WEATHER_MAP_CONFIG, OpenWeatherMapHarvester.class),
                startVerticleIfApplicable(options, ENV_TEST_CONFIG, TestHarvester.class),
                startVerticleIfApplicable(options, ENV_DISCOVERGY_CONFIG, DiscovergyHarvester.class),
                startVerticleIfApplicable(options, ENV_DWD_CONFIG, DwdHarvester.class),
                startVerticleIfApplicable(options, ENV_RMV_CONFIG, RMVHarvester.class)
        );

        return CompositeFuture.join(new ArrayList<>(deployments));
    }

    private Future<Void> startVerticleIfApplicable(DeploymentOptions options, String harvesterKey, Class<? extends AbstractVerticle> clazz) {
        return Future.future(startVerticle -> {
            if (config.containsKey(harvesterKey)) {
                vertx.deployVerticle(clazz.getName(), options, handler -> {
                    if (handler.succeeded()) {
                        startVerticle.complete();
                        log.info("Successfully launched [{}]", clazz.getSimpleName());
                    } else {
                        startVerticle.fail(handler.cause());
                        log.error("Failed to deploy [{}]", clazz.getSimpleName(), handler.cause());
                    }
                });
            } else {
                startVerticle.complete();
                log.info("Skipped deployment of [{}], missing config", clazz.getSimpleName());
            }
        });
    }

    public static void main(String[] args) {
        String[] params = Arrays.copyOf(args, args.length + 1);
        params[params.length - 1] = MainVerticle.class.getName();
        Launcher.executeCommand("run", params);
    }
}


