FROM openjdk:14-jdk

ENV VERTICLE_NAME io.piveau.DataStore.Harvester.MainVerticle
ENV VERTICLE_FILE Harvester.jar

ENV ENV prod

# Set the location of the verticles
ENV VERTICLE_HOME /usr/verticles

# Copy your verticle to the container
COPY target/$VERTICLE_FILE $VERTICLE_HOME/

# Launch the verticle
WORKDIR $VERTICLE_HOME
ENTRYPOINT ["sh", "-c"]
#CMD ["exec vertx run $VERTICLE_NAME -cp $VERTICLE_HOME/*"]
CMD ["exec java $JAVA_OPTS -jar $VERTICLE_FILE"]
